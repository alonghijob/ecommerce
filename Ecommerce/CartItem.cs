﻿using Ecommerce;

namespace Ecommerce
{
    public class CartItem
    {
        public Item Item { get; set; }
        public int Quantity { get; set; }

        public CartItem(Item item, int q)
        {
            this.Item = item;
            this.Quantity = q;
        }
    }
}
