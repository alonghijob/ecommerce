﻿using Ecommerce;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace TestEcommerce
{
    [TestClass]
    public class TestCart
    {
        private static IPromotion _promotion1Buy1Free = new Promotion1Buy1Free();
        private static IPromotion _promotion3Any10 = new Promotion3Any10();

        private static List<Item> _items = new List<Item>{
            new Item(1,"A", 20m)
            ,new Item(2,"B", 4m)
            ,new Item(3,"C", 2m)
            ,new Item(4,"D", 4m)
        };

        private static List<Item> _itemsWithPromotion = new List<Item>{
            new Item(1,"A", 20m, _promotion1Buy1Free)
            ,new Item(2,"B", 4m, _promotion3Any10)
            ,new Item(3,"C", 2m)
            ,new Item(4,"D", 4m, _promotion3Any10)
        };


        [TestMethod]
        public void TestMethodCart()
        {
            var cart = new Cart();

            Assert.AreNotEqual(null, cart.Items);
        }

        [TestMethod]
        public void TestMethodCheckout()
        {
            // initial condition before promotion management
            var cart = new Cart();

            cart.Items.Add( new CartItem( _items[0],2));
            cart.Items.Add(new CartItem(_items[1], 3));
            cart.Items.Add(new CartItem(_items[2], 5));
            cart.Items.Add(new CartItem(_items[3], 2));
            
            Assert.AreEqual(70m, cart.Checkout());
        }


        [TestMethod]
        public void TestMethodCheckoutWithoutPromotion()
        {
            // check return without promotions
            var cart = new Cart();

            cart.Items.Add(new CartItem(_itemsWithPromotion[0], 2));
            cart.Items.Add(new CartItem(_itemsWithPromotion[1], 3));
            cart.Items.Add(new CartItem(_itemsWithPromotion[2], 5));
            cart.Items.Add(new CartItem(_itemsWithPromotion[3], 2));

            var usePromotion = false;

            Assert.AreEqual(70m, cart.Checkout(usePromotion));
        }

        [TestMethod]
        public void TestMethodCheckoutWithPromotion()
        {
            // check return with promotions

            var cart = new Cart();

            cart.Items.Add(new CartItem(_itemsWithPromotion[0], 2));
            cart.Items.Add(new CartItem(_itemsWithPromotion[1], 3));
            cart.Items.Add(new CartItem(_itemsWithPromotion[2], 5));
            cart.Items.Add(new CartItem(_itemsWithPromotion[3], 2));

            var usePromotion = true;

            Assert.AreEqual(48m, actual: cart.Checkout(usePromotion));
        }

    }

}
