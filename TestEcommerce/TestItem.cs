using Ecommerce;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestEcommerce
{
    [TestClass]
    public class TestItem
    {
        [TestMethod]
        public void TestMethodItem()
        {

            var id = 1;
            var name = "A";
            var price = 20m;
            var item = new Item(id, name, price);

            Assert.AreEqual(id,item.Id);
            Assert.AreEqual(name, item.Name);
            Assert.AreEqual(price, item.Price);
        }

        [TestMethod]
        public void TestMethodItemWithPromotion()
        {

            var id = 1;
            var name = "A";
            var price = 20m;

            var promotion = new Promotion1Buy1Free();
            var item = new Item(id, name, price, promotion);

            Assert.AreEqual(id, item.Id);
            Assert.AreEqual(name, item.Name);
            Assert.AreEqual(price, item.Price);
            Assert.AreEqual(promotion, item.Promotion);
        }
    }

}
