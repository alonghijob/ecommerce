﻿using Ecommerce;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestEcommerce
{
    [TestClass]
    public class TestCartItem
    {
        [TestMethod]
        public void TestMethodCartItem()
        {
            var id = 1;
            var name = "A";
            var price = 20m;
            var item = new Item(id, name, price);

            var quantity = 2;

            var cartItem = new CartItem(item, quantity);

            Assert.AreEqual(item, cartItem.Item);
            Assert.AreEqual(quantity, cartItem.Quantity);
        }
    }
}
