﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ecommerce
{

    public class Promotion1Buy1Free : IPromotion
    {
        public Promotion1Buy1Free()
        {
        }

        public decimal ComputePriceItem(int quantity, decimal pricePerItem)
        {
            var mod = quantity % 2;
            return (mod + (quantity / 2)) * pricePerItem;
        }
    }
}
