﻿namespace Ecommerce
{
    public class Promotion3Any10 : IPromotion
    {
        public Promotion3Any10()
        {
        }

        public decimal ComputePriceItem(int quantity, decimal pricePerItem)
        {
            var mod = quantity % 3;
            return mod * pricePerItem + (quantity / 3) * 10;
        }
    }
}
