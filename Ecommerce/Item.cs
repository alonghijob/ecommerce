﻿using System;

namespace Ecommerce
{
    public class Item
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public IPromotion Promotion { get; set; }

        public Item(int id, string name, decimal price)
        {
            Id = id;
            Name = name;
            Price = price;
        }

        public Item(int id, string name, decimal price, IPromotion promotion) : this(id, name, price)
        {
            Promotion = promotion;
        }
    }
}
