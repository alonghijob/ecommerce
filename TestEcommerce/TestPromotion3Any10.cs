﻿using Ecommerce;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestEcommerce
{
    [TestClass]
    public class TestPromotion3Any10
    {
        private static IPromotion _promotion = new Promotion3Any10();

        [TestMethod]
        public void TestMethodComputePriceItemQt3()
        {
            Assert.AreEqual(10m, _promotion.ComputePriceItem(3, 4m));
        }

        [TestMethod]
        public void TestMethodComputePriceItemQt2()
        {
            Assert.AreEqual(8m, _promotion.ComputePriceItem(2, 4m));
        }

        [TestMethod]
        public void TestMethodComputePriceItemQt4()
        {
            Assert.AreEqual(14m, _promotion.ComputePriceItem(4, 4m));
        }

        [TestMethod]
        public void TestMethodComputePriceItemQt6()
        {
            Assert.AreEqual(20m, _promotion.ComputePriceItem(6, 4m));
        }

        [TestMethod]
        public void TestMethodComputePriceItemQt8()
        {
            Assert.AreEqual(28m, _promotion.ComputePriceItem(8, 4m));
        }
    }

}
