﻿using Ecommerce;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TestEcommerce
{
    [TestClass]
    public class TestPromotion1Buy1Free
    {
        private static IPromotion _promotion = new Promotion1Buy1Free();

        [TestMethod]
        public void TestMethodComputePriceItemQt1()
        {
            Assert.AreEqual(20m, _promotion.ComputePriceItem(1, 20m));
        }

        [TestMethod]
        public void TestMethodComputePriceItemQt2()
        {
            Assert.AreEqual(20m, _promotion.ComputePriceItem(2, 20m));
        }


        [TestMethod]
        public void TestMethodComputePriceItemQt3()
        {
            Assert.AreEqual(40m, _promotion.ComputePriceItem(3, 20m));
        }


        [TestMethod]
        public void TestMethodComputePriceItemQt4()
        {
            Assert.AreEqual(40m, _promotion.ComputePriceItem(4, 20m));
        }
    }

}
