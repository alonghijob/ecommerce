﻿namespace Ecommerce
{
    public interface IPromotion
    {

        decimal ComputePriceItem(int quantity, decimal pricePerItem);
    }
}
