﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Ecommerce
{
    public class Cart
    {
        public List<CartItem> Items { get; private set; }

        public Cart()
        {
            Items = new List<CartItem>();
        }

        public decimal Checkout(bool usePromotion = false)
        {
            var total = Items.Sum(p =>
            {
                if (usePromotion && (p.Item.Promotion != null))
                {
                    return p.Item.Promotion.ComputePriceItem(p.Quantity, p.Item.Price);
                }

                return p.Item.Price * p.Quantity;
            });

            return total;
        }
    }
}
